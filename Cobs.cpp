#include "Cobs.h"
#include "Stream.h"

#ifdef COBS_ENABLE_LOG
Cobs::Cobs( Stream& iStream, Adafruit_GFX* iLog ) : _stream( iStream ), _log( iLog ) {
#else
Cobs::Cobs( Stream& iStream ) : _stream( iStream ) {
#endif

  _next_write = _rx_buffer;
  _non_zero_counter = 0;
  _rx_state = RX_WAITING_FRAME;
  _rx_status = RX_STATUS_UNKNOWN; 
}

/*void Cobs::setRXStatus( const uint8_t iState ) {
  _rx_buffer[ RX_BUFFER_STATUS_OFFSET ] = iState;
}

void Cobs::setRXSize( const uint8_t iSize ) {
  _rx_buffer[ RX_BUFFER_SIZE_OFFSET ] = iSize;
}*/

uint8_t Cobs::getRXStatus() const {
  return _rx_status;
}

uint8_t Cobs::getRXSize() const {
  return _rx_size;
}

const uint8_t* Cobs::getRXBuffer() const {
  return _rx_buffer;
}

bool Cobs::read() {
  
  
  bool doBreak = false;
  bool result = false;
  
  // Consume everything in the buffer
  while( _stream.available() && doBreak == false ) {
    uint8_t aByte = _stream.read();
    
    #ifdef COBS_ENABLE_LOG
    if( _log != NULL ) {
      _log->print("RX:");
      _log->write(aByte);
    
      _log->print("|ST:");
      _log->print(_rx_state);
    }
    #endif

    switch( _rx_state ) {
      case RX_WAITING_FRAME:
        
        // 1st byte : if greater than the max packet length start counting the non-0 bytes, but do not write anything in buffer
        if( aByte > RX_PACKET_SIZE ) {
          //display.println("too long");
          _rx_state = RX_IN_FRAME_ERROR;
        } else {
          // Assumed valid : initialise the non \x00 byte counter, and switch to 'in frame'
          _non_zero_counter = aByte;
          _next_write = _rx_buffer;
          _rx_status = RX_STATUS_INCOMPLETE;
          _rx_size = 0;
          _rx_state = RX_IN_FRAME;
        }
        break;
        
      case RX_IN_FRAME:
        _non_zero_counter--;
        
        if( aByte == 0 ) {
          // Sanity check : if aByte == 0 : this is the terminating character.
          // If all went well, non_zero_counter should have reach 0 just now.
          // If it didnt : reject current packet, and consider we are waiting for the next frame
          // (assuming that the \x00 was not itself an error - but we can not check that...)
          if( _non_zero_counter != 0 ) {
            _rx_status = RX_STATUS_COBS_ERROR;
          } else {
            _rx_status = RX_STATUS_OK;
          }

          _rx_size = _next_write - _rx_buffer;
          
          result = true;
          _rx_state = RX_WAITING_FRAME;
          doBreak = true;
        }
        // Non 0 byte found. 
        else {
          // Check for buffer overflow
          if( _next_write - _rx_buffer >= RX_PACKET_SIZE ) {
            _rx_state = RX_IN_FRAME_ERROR;
            _rx_status = RX_STATUS_OVFLW;
            doBreak = true;
            break;
          }
          // Havent reached the end of non-0 characters : simply add the character
          if( _non_zero_counter != 0 ) {
            *_next_write = aByte;
          }
          // if non 0 byte counter reached 0 : time to read its new value and write a \x00
          else {
            _non_zero_counter = aByte;
            // Could add a sanity check here : if non_zero_counter + next_write - cur_buf > RX_PACKET_SIZE : error
            *_next_write = 0;
          }
          ++_next_write;
        }
        
        break;
        
      case RX_IN_FRAME_ERROR:
        // In that state we wait for the next 0 byte.
        if( aByte == 0 ) {
          _rx_state = RX_WAITING_FRAME;
        }
        break;
    }

    #ifdef COBS_ENABLE_LOG
    if( _log != NULL ) {
      _log->print("|Brk:");
      _log->print(doBreak);
      _log->print("|ST:");
      _log->println(_rx_state);
    }
    #endif
    
  }
  return result;
}


