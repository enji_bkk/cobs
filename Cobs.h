#ifndef _AFAURE_COBS_H
#define _AFAURE_COBS_H

#include <inttypes.h>

// From arduino IDE : can not pass compiler flags => only way to affect the sektch and the lib is to define it here
//#define COBS_ENABLE_LOG

#ifdef COBS_ENABLE_LOG
  #include <Adafruit_GFX.h> 
#endif

// Default RX buffer size
// Will store RX_BUFFER_SIZE - 2 bytes of data, state, and size
#ifndef RX_BUFFER_SIZE
  #define RX_BUFFER_SIZE 64
#endif

#define RX_PACKET_SIZE RX_BUFFER_SIZE-2

#define RX_STATUS_OK 0
#define RX_STATUS_INCOMPLETE 1
#define RX_STATUS_COBS_ERROR 2
#define RX_STATUS_CRC_ERROR 3
#define RX_STATUS_OVFLW 4
#define RX_STATUS_UNKNOWN 0xFF

#define RX_WAITING_FRAME 0      // Nothing has been received yet, or we have just received a \x00
#define RX_IN_FRAME 2           // Subsequent bytes received
#define RX_IN_FRAME_ERROR 3     // incomplete frame / buffer overflow (meaning we received garbage)

// Forward declaration
class Stream;

class Cobs {
  public:
    // Constructor : takes the underlying serial library instance as parameter
    // Also convenient for unit tests
    #ifdef COBS_ENABLE_LOG
    Cobs( Stream& iStream, Adafruit_GFX* iLog = NULL );
    #else
    Cobs( Stream& iStream );
    #endif

    // Process all characters in receive buffer. Returns True when something
    // useful is in the buffer
    bool read();

    // Buffer location
    const uint8_t* getRXBuffer() const;

    // Size of the useful data present in the buffer
    uint8_t getRXSize() const;

    // Status of the RX buffer
    uint8_t getRXStatus() const;
  
  private:
    // Set the status and size
    void setRXSize( const uint8_t iSize );

    void setRXStatus( const uint8_t iStatus );

  private:
    // An instance of Stream : should then work with Serial, Wire, ...
    Stream& _stream;

    // RX buffer : -2 because RX_BUFFER_SIZE is like: |cobs_header[1]|data[1..62]|0x00|
    uint8_t _rx_buffer[RX_BUFFER_SIZE - 2];
    // RX metadata
    uint8_t _rx_size;
    uint8_t _rx_status;

    // Members to keep track of the state
    uint8_t* _next_write;
    uint8_t  _non_zero_counter;
    uint8_t  _rx_state;

    #ifdef COBS_ENABLE_LOG
    Adafruit_GFX* _log;
    #endif
};

#endif
