#include "Stream.h"
#include <iostream>

#define _STREAM_LOG

#ifdef _STREAM_LOG
  #define LOG(stuff) std::cout<<stuff;
#else
  #define LOG(stuff) ;
#endif

bool Stream::available() {
  return !empty();
}

uint8_t Stream::read() {
  
  uint8_t result = front();
  pop_front();
  LOG( "returning >>>" << result << "<<<  state after >>>" << available() << "<<<" << std::endl );
  return result;
}

void Stream::push( const std::string& iString) {
  insert( end(), iString.begin(), iString.end() );
}
