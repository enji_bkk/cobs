#include "CppUTest/TestHarness.h"

#include "../Stream.h"


TEST_GROUP(StreamMockTest) {

  Stream* _stream;

  void setup()
  {
    _stream = new Stream();  
  }

  void teardown()
  {
    delete _stream;
  }

};

// Test the initial state
TEST(StreamMockTest, InitState)
{
  CHECK_FALSE( _stream->available() );
}

// Check there are available bytes after adding data
TEST(StreamMockTest, AvailableBytes) {
  _stream->push("TEST");
  CHECK( _stream->available() );
}                                                   

// Check characters are read
TEST(StreamMockTest, ReadBytes) {
  _stream->push("TO");
  CHECK( _stream->read() == 'T' );
  CHECK( _stream->read() == 'O' );
}

// Check available() return false after reading all data
TEST(StreamMockTest, NotAvailableAfterAllRead) {
  _stream->push("TOTO");
  _stream->read();
  _stream->read();
  _stream->read();
  _stream->read();
  CHECK_FALSE(_stream->available() );
}

// Check works with \0 too
TEST(StreamMockTest, ReadNullBytes) {
  _stream->push( std::string( "TO\0POTATO", 9 ) );
  CHECK( _stream->read() == 'T' );
  CHECK( _stream->read() == 'O' );
  CHECK( _stream->read() == '\0' );
  CHECK( _stream->read() == 'P' );
}
                                                         
