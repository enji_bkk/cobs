#ifndef _MOCK_STREAM_H
#define _MOCK_STREAM_H

#include <deque>
#include <string>
#include <stdint.h>

class Stream : public std::deque<uint8_t> {
  
  public:
    // The interface to mock
    bool available();
    uint8_t read();

  public:
    // methods to setup the mock
    void push( const std::string& iString );
};

#endif

