#include "CppUTest/TestHarness.h"

#include <string>

#include "Cobs.h"
#include "Stream.h"

TEST_GROUP(TestCobsDecode) {

  Stream* _stream;
  Cobs*   _cobs;

  void setup()
  {
    _stream = new Stream();
    _cobs = new Cobs( *_stream );
  }

  void teardown()
  {
    delete _cobs;
    delete _stream;
  }

};

// Incomplete packet : no closing \x00
TEST(TestCobsDecode, DecodePacketNotReceived)
{
  _stream->push( std::string("\x05TOTO", 5) );
  bool bReadSomething = _cobs->read();

  CHECK_FALSE( bReadSomething );
  BYTES_EQUAL( RX_STATUS_INCOMPLETE, _cobs->getRXStatus() );
}

// Complete packet received.
TEST(TestCobsDecode, DecodePacketReceived)
{
  _stream->push( std::string("\x05TOTO\x00", 6) );
  bool bReadSomething = _cobs->read();

  // Check the status is correct : bReadSomething == true, as
  // we have just completed a packet
  CHECK( bReadSomething );
  BYTES_EQUAL( RX_STATUS_OK, _cobs->getRXStatus() );

  // Check the lenght of decoded data in the rx buffer
  BYTES_EQUAL( 4, _cobs->getRXSize() );

  // Check the decoded value present in buffer
  MEMCMP_EQUAL( "TOTO", _cobs->getRXBuffer(), 4 );
 
}

// Complete packet, but in 2 calls to Cobs::read()
// Goal is to check that we get the same result as DecodePacketReceived
TEST(TestCobsDecode, DecodePacketReceivedIn2Chunks)
{
  bool bReadSomething = false;
  
  _stream->push( std::string("\x05TOT", 4) );
  bReadSomething = _cobs->read();
  CHECK_FALSE( bReadSomething );
  BYTES_EQUAL( RX_STATUS_INCOMPLETE, _cobs->getRXStatus() );

  _stream->push( std::string("O\x00", 2) );
  bReadSomething = _cobs->read();

  // Check the status is correct : bReadSomething == true, as
  // we have just completed a packet
  CHECK( bReadSomething );
  BYTES_EQUAL( RX_STATUS_OK, _cobs->getRXStatus() );

  // Check the lenght of decoded data in the rx buffer
  BYTES_EQUAL( 4, _cobs->getRXSize() );

  // Check the decoded value present in buffer
  MEMCMP_EQUAL( "TOTO", _cobs->getRXBuffer(), 4 );

}

TEST(TestCobsDecode, DecodePacketWithMiddleNullCharacter)
{
  _stream->push( std::string("\x05TOTO\x03TU\x00", 9) );
  bool bReadSomething = _cobs->read();

  // Check the status is correct : bReadSomething == true, as
  // we have just completed a packet
  CHECK( bReadSomething );
  BYTES_EQUAL( RX_STATUS_OK, _cobs->getRXStatus() );

  // Check the lenght of decoded data in the rx buffer
  BYTES_EQUAL( 7, _cobs->getRXSize() );

  // Check the decoded value present in buffer
  MEMCMP_EQUAL( "TOTO\x00TU", _cobs->getRXBuffer(), 7 );

}

TEST(TestCobsDecode, DecodePacketWithLeadingNullCharacter)
{
  _stream->push( std::string("\x01\x05TOTO\x00", 7) );
  bool bReadSomething = _cobs->read();

  // Check the status is correct : bReadSomething == true, as
  // we have just completed a packet
  CHECK( bReadSomething );
  BYTES_EQUAL( RX_STATUS_OK, _cobs->getRXStatus() );

  // Check the lenght of decoded data in the rx buffer
  BYTES_EQUAL( 5, _cobs->getRXSize() );

  // Check the decoded value present in buffer
  MEMCMP_EQUAL( "\x00TOTO", _cobs->getRXBuffer(), 5 );

}

TEST(TestCobsDecode, DecodePacketWithTrailingNullCharacter)
{
  _stream->push( std::string("\x05TOTO\x01\x00", 7) );
  bool bReadSomething = _cobs->read();

  // Check the status is correct : bReadSomething == true, as
  // we have just completed a packet
  CHECK( bReadSomething );
  BYTES_EQUAL( RX_STATUS_OK, _cobs->getRXStatus() );

  // Check the lenght of decoded data in the rx buffer
  BYTES_EQUAL( 5, _cobs->getRXSize() );

  // Check the decoded value present in buffer
  MEMCMP_EQUAL( "TOTO\x00", _cobs->getRXBuffer(), 5 );

}

// 62 byte packet
TEST(TestCobsDecode, LongestPacket)
{
  _stream->push( std::string("\x08TOOOOOO", 8) ); // 07
  _stream->push( std::string("\x08TTOOOOO", 8) ); // 15
  _stream->push( std::string("\x08TTTOOOO", 8) ); // 23
  _stream->push( std::string("\x08TTTTOOO", 8) ); // 31
  _stream->push( std::string("\x08TTTTTOO", 8) ); // 39
  _stream->push( std::string("\x08TTTTTTO", 8) ); // 47
  _stream->push( std::string("\x08TTTTTTT", 8) ); // 55
  _stream->push( std::string("\x07RRRRRR\x00", 8) ); // 62

  bool bReadSomething = _cobs->read();

  CHECK( bReadSomething );
  BYTES_EQUAL( RX_STATUS_OK, _cobs->getRXStatus() );
  BYTES_EQUAL( 62, _cobs->getRXSize() );
  MEMCMP_EQUAL( "TOOOOOO\x00TTOOOOO\x00TTTOOOO\x00TTTTOOO\x00TTTTTOO\x00TTTTTTO\x00TTTTTTT\x00RRRRRR", _cobs->getRXBuffer(), 62 );

}

