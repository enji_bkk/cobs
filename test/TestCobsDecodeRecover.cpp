#include "CppUTest/TestHarness.h"

#include <string>

#include "Stream.h"
#include "Cobs.h"

TEST_GROUP(TestCobsDecodeRecover) {

  Stream* _stream;
  Cobs*   _cobs;

  void setup()
  {
    _stream = new Stream();
    _cobs = new Cobs( *_stream );
  }

  void teardown()
  {
    delete _cobs;
    delete _stream;
  }

};

// First received byte > 63, then \x00 to terminate package 
// Nothing is considered received
TEST(TestCobsDecodeRecover, FirstByteGreaterThan63ThenValidPacket)
{
  _stream->push( std::string("\x64\x00\x05TOTO\x00", 8) );
  bool bReadSomething = _cobs->read();

  CHECK( bReadSomething );
  BYTES_EQUAL( RX_STATUS_OK, _cobs->getRXStatus() );
  BYTES_EQUAL( 4, _cobs->getRXSize() );
  MEMCMP_EQUAL( "TOTO", _cobs->getRXBuffer(), 4 );
}

// First byte : 0x05. Expect 4 characters non-0 characters at least
TEST(TestCobsDecodeRecover, Unexpected0x00ThenValidPacket)
{
  _stream->push( std::string("\x05to\x00\x05TOTO\x00", 10) );
  bool bReadSomething = _cobs->read();
  // read stops after reaching \x00 : read again to get the next packet
  bReadSomething = _cobs->read();

  CHECK( bReadSomething );
  BYTES_EQUAL( RX_STATUS_OK, _cobs->getRXStatus() );
  BYTES_EQUAL( 4, _cobs->getRXSize() );
  MEMCMP_EQUAL( "TOTO", _cobs->getRXBuffer(), 4 );
}

// Overflow : valid cobs, but size reaches 63
// First byte : 0x05. Expect 4 characters non-0 characters at least
TEST(TestCobsDecodeRecover, OverflowThenValid)
{
  _stream->push( std::string("\x08TOOOOOO", 8) ); // 07
  _stream->push( std::string("\x08TTOOOOO", 8) ); // 15
  _stream->push( std::string("\x08TTTOOOO", 8) ); // 23
  _stream->push( std::string("\x08TTTTOOO", 8) ); // 31
  _stream->push( std::string("\x08TTTTTOO", 8) ); // 39
  _stream->push( std::string("\x08TTTTTTO", 8) ); // 47
  _stream->push( std::string("\x08TTTTTTT", 8) ); // 55
  _stream->push( std::string("\x08RRRRRRR", 8) ); // 63
  _stream->push( std::string("\x00", 1) ); // 64
  _stream->push( std::string("\x05TOTO\x00", 6) );

  bool bReadSomething = _cobs->read();
  bReadSomething = _cobs->read();

  CHECK( bReadSomething );
  BYTES_EQUAL( RX_STATUS_OK, _cobs->getRXStatus() );
  BYTES_EQUAL( 4, _cobs->getRXSize() );
  MEMCMP_EQUAL( "TOTO", _cobs->getRXBuffer(), 4 );
}

