#include "CppUTest/TestHarness.h"

#include <string>

#include "Stream.h"
#include "Cobs.h"

TEST_GROUP(TestCobsDecodeError) {

  Stream* _stream;
  Cobs*   _cobs;

  void setup()
  {
    _stream = new Stream();
    _cobs = new Cobs( *_stream );
  }

  void teardown()
  {
    delete _cobs;
    delete _stream;
  }

};

// First received byte > 63: would yield a packet longer than 62 bytes.
// We seem to have missed the start of data => straight to IN_FRAME_ERROR
TEST(TestCobsDecodeError, FistByteGreaterThan63)
{
  _stream->push( std::string("\x64", 1) );
  bool bReadSomething = _cobs->read();

  CHECK_FALSE( bReadSomething );
  BYTES_EQUAL( RX_STATUS_UNKNOWN, _cobs->getRXStatus() );
}

// First received byte > 63, then \x00 to terminate package 
// Nothing is considered received
TEST(TestCobsDecodeError, FistByteGreaterThan63Then0x00)
{
  _stream->push( std::string("\x64", 1) );
  bool bReadSomething = _cobs->read();

  CHECK_FALSE( bReadSomething );
  BYTES_EQUAL( RX_STATUS_UNKNOWN, _cobs->getRXStatus() );

  _stream->push( std::string("\x00", 1) );
  bReadSomething = _cobs->read();

  CHECK_FALSE( bReadSomething );
  BYTES_EQUAL( RX_STATUS_UNKNOWN, _cobs->getRXStatus() );

}

// First byte : 0x05. Expect 4 characters non-0 characters at least
TEST(TestCobsDecodeError, Unexpected0x00)
{
  _stream->push( std::string("\x05to\x00", 4) );
  bool bReadSomething = _cobs->read();

  CHECK( bReadSomething );
  BYTES_EQUAL( RX_STATUS_COBS_ERROR, _cobs->getRXStatus() );
}

// Overflow : valid cobs, but size reaches 63
// First byte : 0x05. Expect 4 characters non-0 characters at least
TEST(TestCobsDecodeError, Overflow)
{
  _stream->push( std::string("\x08TOOOOOO", 8) ); // 07
  _stream->push( std::string("\x08TTOOOOO", 8) ); // 15
  _stream->push( std::string("\x08TTTOOOO", 8) ); // 23
  _stream->push( std::string("\x08TTTTOOO", 8) ); // 31
  _stream->push( std::string("\x08TTTTTOO", 8) ); // 39
  _stream->push( std::string("\x08TTTTTTO", 8) ); // 47
  _stream->push( std::string("\x08TTTTTTT", 8) ); // 55
  _stream->push( std::string("\x08RRRRRRR", 8) ); // 63
  _stream->push( std::string("\x00", 1) ); // 64

  bool bReadSomething = _cobs->read();

  CHECK_FALSE( bReadSomething );
  BYTES_EQUAL( RX_STATUS_OVFLW, _cobs->getRXStatus() );
}


// Start with invalid packet (1st byte greater than 63)
// followed by valid packet
// When in error : wait till 0x00 to 'resynchronize'
TEST(TestCobsDecodeError, FirstByteGreaterThan62ThenValidPacket)
{
  _stream->push( std::string("\x63\x05toto\x00", 7) );
  bool bReadSomething = _cobs->read();

  CHECK_FALSE( bReadSomething );
  BYTES_EQUAL( RX_STATUS_UNKNOWN, _cobs->getRXStatus() );

}



